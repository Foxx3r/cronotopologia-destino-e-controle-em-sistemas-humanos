# Cronotopologia: Destino e Controle em Sistemas humanos
Este repositório é um resumo do livro Destiny and Control in Human Systems: Studies in the Interactive Connectedness of Time (Chronotopology) do matemático, físico e ciberneticista Charles Musès. 

# Por que ler Musès?
Musès era uma pessoa genial. Ele era físico, matemático, psicólogo, oceanólogo, sociólogo, ciberneticista, biólogo, programador, egiptólogo (até mesmo descobriu várias pirâmides), filósofo, esotérico, ocultista, ativista ecológico, criou uma religião xamanística (em Lion Path), foi requisitado pelo governo italiano para escrever o obituário oficial do pai da cibernética (Norbert Wiener – e o mesmo publicou escritos não-publicados do mesmo), privilegia a tipologia nas ciências, gostava de teoria musical, sabia hieróglifos e sânscrito, sempre se preocupou em deixar suas teorias matemáticas computáveis (i.e, construtivistas), chegou às mesmas conclusões que Bohm e Pribrim independentemente em Consciousness and Reality, era contribuidor do Impacto da Ciência na Sociedade da UNESCO, fundador-editor do jornal britânico Kybernetes, atribui enorme importância a poesia e filosofa sobre a metáfora, era membro do conselho editorial do International Journal of Bio-Medical Computing, foi convidado duas vezes pela IIASA (International Institute for Applied Systems Analysis), deu um seminário super aclamado na NASA e ainda foi esquecido em grande parte pela internet (embora tenha importantes contribuições para a matemática dos quaternions, octonions e métodos de computação que foram implementados mais recentemente). Há algum motivo a mais para resgatarmos e vingar esse gênio tão incompreendido?

Além dele ter dialogado com as pessoas mais inteligentes do século passado, sendo um amigo próximo de Roddenberry (criador de Star Trek), Halmos (famoso matemático), Stephen Smale (laureado matemático), Wigner (laureado físico), Ralph Abraham (matemático de sistemas dinâmicos e teoria do caos), John Casti (importante teórico de sistemas e complexidades), Schützenberger (importante matemático, médico e biólogo), Francis Huxley (botânico e antropólogo), Joseph Campbell (estudioso da mitologia), Jeffrey Burton Russell (historiador do medieval), George Klir (cientista da computação), Philip Jones (diretor do escritório americano de Kluwer-Nijhoff), David Bohm (físico quântico), Pribrim (psicólogo e professor em Harvard) e Ashby e McCulloch (ambos importantes cientistas da tradição cibernética, sendo professor de Ashby).

O meu motivo de estudá-lo é entender o projeto [FED](https://dialectics.org) ([1](https://feddialectics-miguel.blogspot.com)), que quanto mais eu leio à luz de Musès, mais compreendo.

**Quem Musès critica?**

Husserl, Norbert Wiener, David Berlinski e Von Bertalanffy (de uma maneira mais positiva que o geral), Carl Gustav Jung, Bohr, Bertrand Russell, Halmos e Bergson.

**Quais eram as idéias de Musès?**

Ele dizia que haviam dimensões negativas (-1 sendo a dimensão do tempo e como -1 é o maior número negativo, é a maior negadimensão) e uma dimensão zero (onde só haveria espaço), a 1 seria da duração e portanto todo espaço estaria incluído em um ponto do tempo. Para Musès, o presente depende não só do passado, mas também do futuro (e portanto, não pode ser expresso em números reais, apenas em hipernúmeros) e quebra o determinismo com essa ideia. Expressa uma teoria do livre arbítrio ciberneticamente. Era anti-fissão nuclear (pois é um empreendimento ecológico perigoso).

Defende uma abordagem matemática não-linear e qualitativa. Critica os semanticistas (por não enxergarem a importância da sintaxe ao relacionar, organizar e articular) e os sintaticistas (por não buscarem o significado dos símbolos e tornar estes símbolos arbitrários). Nenhum símbolo, por mais abstrato que seja, é destituído de algum significado. Desenvolvimento da linguagem sintática e insight não estão separados (de certa forma, lembrando a inseparação entre prova-refutações de Lakatos). Defende que primeiro devemos sair da realidade para depois fazer a teoria (ao contrário do formalismo, que deforma a realidade em si mesmo). Depois deste processo, Musès apresenta uma teoria formal para entendermos a realidade. Vê a lógica como uma área da topologia (pois ela é "uma representação simbólica ou reescrita da estrutura, isto é, a autoconectividade de alguma realidade").

Define o ser humano como homo symbolens (pois somos os únicos animais na natureza que atribuem significado simbólico a algo) ou homo diabolicus (somos os únicos animais na natureza que podem ocultar o significado simbólico de algo por meio da socialização "micro-xenofóbica" ou criptografia). Condena o diabolon/jargão por sua fraca capacidade de inovação e integração. Propõe uma teoria ontológica-linguística dos pararoxos que lembra e muito a semântica da verdade de Tarski (principalmente no trecho "o signo é sobre algo externo, e o símbolo é interno, e quanto mais interno for o símbolo, mais arbitrário ele será"), num aspecto bem menos formalista, baseada na noção de tempo. Para Musès, o que sustenta o mundo não é a predação da natureza (a qual realizam certos humanos com o desmatamento e poluição), mas a simbiose.

Desenvolve uma linguagem radial (isto é, uma linguagem não-linear, onde a sequência não importa), chamada psiglifo. Vê a linguagem como embora sendo linear, dependente do tempo (e isso também tem base em sua crítica às 3 relações da aritmética (comutatividade, associatividade e distributividade) que depende do tempo (cronotopologia), embora sejam expressos de forma linear). Vê a ocorrência como uma recorrência que é diferente da ocorrência anterior (pois a história gira a partir de um mesmo eixo, e ela pode voltar a esses pontos, mas quando ela volta, ela volta mudada (por exemplo, o argumento do rio de Heráclito), por isso ele chama tais movimentos helicoidais (que sempre giram em torno de um mesmo lugar, um mesmo eixo) de movimento axial irreversível). Diz que o advento da computação tornou necessário revisar nossos conhecimentos sobre sintaxe e gramática, para que computadores sejam capazes de capturá-las e agir sobre elas. Ele vê computadores como em par de igualdade com os seres humanos (porém, também vê essas máquinas como mecânicas demais e com significado restrito de gravação, tornando-as impossível superar o ser humano em complexidade e terem algum livre-arbítrio ou sentimentos) por eles serem essencialmente temporais e condena a visão de que elas estariam além de nós.

A metáfora (parabólica), ao contrário do symbolen (que reforça o significado) ou o diabolon (que desvia o significado), é a junção dos 2, onde por meio de comparações, se chega a uma identidade em um nível completamente superior com transferência de significado, onde ocorre no nível semântico e não sintático, portanto, o mundo é metáfora (daí vem sua adoração pela poesia, pois poetas reconhecem isso, e também aliado a sua visão de que a linguagem poética chega mais facilmente a linguagens radiais e a prova disso é uma comparação que o mesmo faz entre a etimologia em árabe e alemão) e a própria álgebra é um grande sistema metafórico. A visão de Musès sobre a matemática é a clássica epistemológica platônica. Musès condena a alegoria. Musès critica a visão de que existem fenômenos acausais (portanto, tal fenômeno pode ser uma causalidade não-linear). Compartilha com Hermann Weyl a visão de que espaço e tempo são bem diferentes (embora estejam conectados).

**Quais os conceitos originais principais de Musès?**

- Symbolen (ressonante)
- Diabolon (antiressonante/códon/signo/sinal/linguagem arbitrária)
- Parabólico (uníssono/metáfora)
- Helicoide
- Axial
- Homeomorfismo
- Falácia da ilegitimidade lexicográfica
- Linguagem radial (e.g, psiglifo)
- Hipernúmeros
- Cronotopologia
- Auto-conectividade
- Máquinas Kronos (computadores)
- Cronossimbiose
- Cronossistema
- Homo symbolens
- Homo diabolicus
- Negadimensão
- Pontos assintóticos
- Quatro Equilíbrios Cosmo-Ecológicos Fundamentais (ligação proton-neutron, massa e distância do Sol e da Terra, regulação de acidez/alcalinidade, excedente de carbono)

**O que é cronotopologia?**

Primeiramente, a cronotopologia é essencial a sistemas humanos, pois estes dependem também de variáveis futuras. Assim, torna-se uma ferramenta poderosa para analisar fenômenos psicossociais. Em suas próprias palavras, a cronotopologia é a visão de que o tempo é "como um sistema auto-ressonante de interconexões qualitativas e autoconectividade".

**O que são hipernúmeros?**

Hipernúmeros são parte do projeto de Charles Musès sobre a formalização da cronotopologia e tem uma motivação puramente física. Embora pareça a análise não-padrão, hipernúmeros são construídos topologicamente, não usando conjuntos, e não existem infinitesimais. Os números reais e os complexos são subconjuntos dos hipernúmeros (e o número imaginário i é um hipernúmero), e hipernúmeros são computáveis. São 10 hipernúmeros desempenhando a mais diversa variedade de funções (cada um com sua própria aritmética e geometria), como o hipernúmero Ω que representa a inefabilidade simbólica da linguagem linear. Muitos são abstrações de conceitos matemáticos inexpressáveis, tal como o hipernúmero ε, que é a representação simbólica da raiz quadrada de 1 que não é ±1 (uma preocupação originalmente presente em William Clifford), ou o hipernúmero que representa a raiz quadrada de 0 que não é 0. Mark Burgin expressa muito bem a importância física dos hipernúmeros: com eles, é possível construir conceitos chamados "extrafunções", que resolvem equações diferenciais parciais não-lineares anteriormente irresolvíveis, porque hipernúmeros evitam o processo de fazer renormalizações pois eles nos orientam a como lidar com infinitos.

Note que M-álgebra, criado por Musès, não se refere aos níveis de hipernúmeros. que foi uma área de pesquisa de Musès para investigar sedênions cônicos de 16 dimensões nos hipernúmeros e em dimensões maiores.
